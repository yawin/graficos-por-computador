#include "definitions.h"
#include "load_obj.h"
#include "camera.h"
#include "light.h"
#include <GL/glut.h>
#include <stdio.h>
#include <math.h>

extern object3d * _first_object;
extern object3d * _selected_object;
extern object3d * _first_camera;
extern object3d * _selected_camera;

extern object3d * _selected_light;
extern int _selected_light_index;

extern GLdouble _window_ratio;
extern GLdouble _ortho_x_min,_ortho_x_max;
extern GLdouble _ortho_y_min,_ortho_y_max;
extern GLdouble _ortho_z_min,_ortho_z_max;

#define E_APAGADO   0
#define E_TRASLADAR 1
#define E_ROTAR     2
#define E_ESCALAR   3

#define E_LOCAL     0
#define E_GLOBAL    1

#define E_OBJ       0
#define E_CAM       1
#define E_LUZ       2

int edit_mode = E_APAGADO;
int system_mode = E_GLOBAL;
int transform_mode = E_OBJ;
int cam_transform_mode = MODO_VUELO;

extern int view_normals;

void transforma(object3d* obj, float *matrix);
void transformaLuz(object3d* obj, float *matrix);
void transformaCamara(object3d* obj, float *matrix);

void eliminar_objeto(object3d *optr)
{
  unsigned int i,j;
  for(i = 0; i < optr->num_faces; i++)
  {
      free(optr->face_table[i].vertex_table);
  }

  free(optr->face_table);
  free(optr->vertex_table);

  free(optr);
}
/**
 * @brief This function just prints information about the use
 * of the keys
 */
void print_help(){
    printf("\nPractica de la asignatura GC. Este programa cambia y visualiza objetos en 3D.\n");
    printf("\n");
    printf("Funciones principales\n");
    printf("<?>      Enseñar ayuda \n");
    printf("<ESC>    Salir del programa \n");
    printf("<F, f>   Cargar un objeto\n");
    printf("<r, R>   Cargar un objeto aleatorio\n");
    printf("<q ,Q>   Mostrar/ocultar normales del objeto\n");
    printf("<o, O>   Activar modo objeto\n");
    printf("<k, K>   Activar modo camara\n");
    printf("<a, A>   Activar modo luz\n");
    printf("<TAB>    Elegir un objeto ya cargado\n");
    printf("<SUPR>   Borra el objeto elegido\n");
    printf("\n");
    printf("Funciones del modo objeto\n");
    printf("    <g, G>    Sistema de referencia global\n");
    printf("    <l, L>    Sistema de referencia del objeto\n");
    printf("    <m, M>    Trasladar el objeto seleccionado\n");
    printf("    <b, B>    Rotar el objeto seleccionado\n");
    printf("    <t, T>    Escalar el objeto seleccionado\n");
    printf("    <Flechas> Aplicar la transformacion activa\n");
    printf("    <CTRL z>  Deshacer la ultima transformacion\n");
    printf("    <->       Agrandar la escala del objeto\n");
    printf("    <+>       Disminuir la escala del objeto\n");
    printf("\n");
    printf("Funciones del modo camara\n");
    printf("    <G, g>    Camara en modo analisis\n");
    printf("    <L, l>    Camara en modo vuelo\n");
    printf("    <P, p>    Cambio del tipo de proyeccion: ortografica / perspectiva\n");
    printf("    <M, m>    Trasladar la camara seleccionada\n");
    printf("    <B, b>    Rotar la camara seleccionada\n");
    printf("    <T, t>    Modificar el volumen de vision de la cmaara seleccionada\n");
    printf("    <Flechas> Aplicar la transformacion activa\n");
    printf("    <->       Agrandar el area de visualizacion\n");
    printf("    <+>       Disminuir el area de visualizacion\n");
    printf("    <c>       Pasar a la siguiente camara de la lista\n");
    printf("    <C>       Visualizar lo que ve el objeto seleccionado\n");
    printf("\n");
    printf("Funciones del modo luz\n");
    printf("    <M, m>    Trasladar la luz seleccionada\n");
    printf("    <B, b>    Rotar la luz seleccionada\n");
    printf("    <Flechas> Aplicar la transformacion activa\n");
    printf("    <->       Agrandar el angulo de apertura de la luz seleccionada (en caso de que sea SPOT)\n");
    printf("    <+>       Disminuir el angulo de apertura de la luz seleccionada (en caso de que sea SPOT)\n");
    printf("    <F1..F8>  Encender/apagar luz\n");
    printf("    <F9>      Activar/desactivar iluminación\n");
    printf("    <F12>     Cambiar de tipo de iluminación del objeto seleccionado\n");
    printf("    <1..8>    Seleccionar luz\n");
    printf("    <0>       Crear luz en el slot seleccionado\n");
    printf("\n\n");
}

void print_estado()
{
  printf("\n- Estado ------------------------\n");
  printf("Controlando: %s\n", (transform_mode == E_OBJ) ? "OBJETO" : (transform_mode == E_CAM) ? "CAMARA" : "LUZ");
  if(transform_mode == E_OBJ)
  {
    printf("Sistema de referencia: %s\n", (system_mode == E_GLOBAL) ? "GLOBAL" : "LOCAL");
  }
  else if(transform_mode == E_OBJ)
  {
    printf("Sistema de control: %s\n", (cam_transform_mode == MODO_VUELO) ? "VUELO" : (cam_transform_mode == MODO_ANALISIS) ? "ANALISIS" : "FIJA EN OBJECTO");
  }
  else
  {
    printf("Luz seleccionada: %d\n", _selected_light_index);
    if(_selected_light != 0)
    {
      printf("Cambio de estado: F%d\n", _selected_light->light->lightIndex + 1);
      printf("Tipo de luz: %s\n", (_selected_light->light->lightType == L_POINT) ? "L_POINT" : (_selected_light->light->lightType == L_DIRECTIONAL) ? "L_DIRECTIONAL" : "L_SPOT");
    }
    else
    {
      printf("No hay luz asignada a esta ranura\n");
    }
  }
  printf("\n");

  printf("Transformacion: %s\n", (edit_mode == E_APAGADO) ? "APAGADO" : (edit_mode == E_TRASLADAR) ? "TRASLACION" : (edit_mode == E_ROTAR) ? "ROTACION" : "ESCALA");
  printf("Proyeccion: %s\n", (_selected_camera->cam->proyeccion == CAM_ORTHOGONAL) ? "ORTOGONAL" : "PERSPECTIVA");
  printf("\n");
}

/**
 * @brief Callback function to control the basic keys
 * @param key Key that has been pressed
 * @param x X coordinate of the mouse pointer when the key was pressed
 * @param y Y coordinate of the mouse pointer when the key was pressed
 */
void mira()
{
    if(cam_transform_mode == MODO_ANALISIS)
    {
      float dist;

      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // Echamos para atrás la cámara
        do
        {
          glMatrixMode(GL_MODELVIEW);
          glLoadMatrixf(_selected_camera->matrix_table->value);
          glTranslatef(0.0, 0.0, -KG_STEP_MOVE);
          matriz *aux = malloc(sizeof (matriz));
          glGetFloatv(GL_MODELVIEW_MATRIX, aux->value);
          aux->next = _selected_camera->matrix_table;
          _selected_camera->matrix_table = aux;

          dist = sqrt(pow(_selected_camera->matrix_table->value[12] - _selected_object->matrix_table->value[12], 2) +
                            pow(_selected_camera->matrix_table->value[13] - _selected_object->matrix_table->value[13], 2) +
                            pow(_selected_camera->matrix_table->value[14] - _selected_object->matrix_table->value[14], 2));
        } while(dist <= 10* KG_STEP_MOVE);
      //
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      mirarObjeto(_selected_camera, _selected_object);
    }
}

void insertObjectInList(object3d* obj)
{
  obj->next = _first_object;
  _first_object = obj;
  _selected_object = _first_object;
  obj->matrix_table = malloc(sizeof (matriz));

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glGetFloatv(GL_MODELVIEW_MATRIX, _first_object->matrix_table->value);

  //_first_object->matrix_table->value;
}

void keyboard(unsigned char key, int x, int y)
{
    int r = rand()%7;
    char* fname = malloc(sizeof (char)*128); /* Note that scanf adds a null character at the end of the vector*/
    int read = 0;
    object3d *auxiliar_object = 0;
    GLdouble wd,he,midx,midy;

    switch (key) {
    /******************************************************************/
    // Temporal
      case 'r':
      case 'R':
        {
          char fnam[7][128] = {
                   "res/deathstr.obj",
                   "res/destroyr.obj",
                   "res/esfera.obj",
                   "res/intercep.obj",
                   "res/mirua.obj",
                   "res/r_falke.obj",
                   "res/x_wing.obj"
            };

            auxiliar_object = (object3d *) malloc(sizeof (object3d));
            read = read_wavefront(fnam[r], auxiliar_object);
            switch (read)
            {
              /*Errors in the reading*/
              case 1:
                  printf("%s: %s\n", fname, KG_MSSG_FILENOTFOUND);
                  break;
              case 2:
                  printf("%s: %s\n", fname, KG_MSSG_INVALIDFILE);
                  break;
              case 3:
                  printf("%s: %s\n", fname, KG_MSSG_EMPTYFILE);
                  break;
              /*Read OK*/
              case 0:
                  /*Insert the new object in the list*/
                  insertObjectInList(auxiliar_object);
                  printf("%s\n",KG_MSSG_FILEREAD);
                  printf("Cargado fichero: %s\n", fnam[r]);
                  break;
            }
            mira();
            print_estado();
          }
          break;

        case 'q':
        case 'Q':
          view_normals = (view_normals == 0) ? 1 : 0;
          break;
    //
    //////////////////////////////////////////////////////////////////////
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
        selectLight(key);
        print_estado();
        break;

    case '0':
        createLight();
        print_estado();
        break;

    case 'f':
    case 'F':
        /*Ask for file*/
        printf("%s", KG_MSSG_SELECT_FILE);
        scanf("%s", fname);
        /*Allocate memory for the structure and read the file*/
        auxiliar_object = (object3d *) malloc(sizeof (object3d));
        read = read_wavefront(fname, auxiliar_object);
        switch (read) {
        /*Errors in the reading*/
        case 1:
            printf("%s: %s\n", fname, KG_MSSG_FILENOTFOUND);
            break;
        case 2:
            printf("%s: %s\n", fname, KG_MSSG_INVALIDFILE);
            break;
        case 3:
            printf("%s: %s\n", fname, KG_MSSG_EMPTYFILE);
            break;
        /*Read OK*/
        case 0:
            /*Insert the new object in the list*/
            insertObjectInList(auxiliar_object);
            printf("%s\n",KG_MSSG_FILEREAD);
            break;
        }
        mira();
        print_estado();
        break;

    case 9: /* <TAB> */
        if(_first_object != 0)
        {
          _selected_object = _selected_object->next;
          /*The selection is circular, thus if we move out of the list we go back to the first element*/
          if (_selected_object == 0) _selected_object = _first_object;
          mira();
        }
        break;

    case 127: /* <SUPR> */
        if(_selected_object != 0)
        {
          /*Erasing an object depends on whether it is the first one or not*/
          if (_selected_object == _first_object)
          {
              /*To remove the first object we just set the first as the current's next*/
              _first_object = _first_object->next;
              /*Once updated the pointer to the first object it is save to free the memory*/
              eliminar_objeto(_selected_object);
              /*Finally, set the selected to the new first one*/
              _selected_object = _first_object;

              // Si ya no hay más objetos desactivamos el modo análisis
              if(cam_transform_mode == MODO_ANALISIS && _selected_object == 0)
              {
                cam_transform_mode = MODO_VUELO;
              }
          } else {
              /*In this case we need to get the previous element to the one we want to erase*/
              auxiliar_object = _first_object;
              while (auxiliar_object->next != _selected_object)
                  auxiliar_object = auxiliar_object->next;
              /*Now we bypass the element to erase*/
              auxiliar_object->next = _selected_object->next;
              /*free the memory*/
              eliminar_objeto(_selected_object);
              /*and update the selection*/
              _selected_object = auxiliar_object;
          }
          mira();
        }
        break;

    case '-':
        if(_selected_object != 0 && transform_mode == E_OBJ)
        {
          int aux_s = system_mode;
          system_mode = E_LOCAL;
          int aux_e = edit_mode;
          edit_mode = E_ESCALAR;

          float matrix[3] = {0.5, 0.5, 0.5};
          transforma(_selected_object, matrix);

          system_mode = aux_s;
          edit_mode = aux_e;
        }
        else if(_selected_camera != 0 && transform_mode == E_CAM)
        {
            float matrix[3] = {1/KG_STEP_ZOOM, 1/KG_STEP_ZOOM, 0.0};

            int edit_mode_aux = edit_mode;
            edit_mode = E_ESCALAR;
            transformaCamara(_selected_camera, matrix);
            edit_mode = edit_mode_aux;
        }
        else if(_selected_light != 0 && _selected_light->light->lightType == L_SPOT)
        {
          if(_selected_light->light->angle > 0)
          {
            _selected_light->light->angle -= KG_STEP_LIGHT_ANGLE;
            if(_selected_light->light->angle < KG_STEP_LIGHT_ANGLE) {_selected_light->light->angle = KG_STEP_LIGHT_ANGLE;}
            glLightf(_selected_light->light->slot, GL_SPOT_CUTOFF, _selected_light->light->angle);
          }
        }
        break;

    case '+':
      if(transform_mode == E_OBJ)
      {
        int aux_s = system_mode;
        system_mode = E_LOCAL;
        int aux_e = edit_mode;
        edit_mode = E_ESCALAR;

        float matrix[3] = {2.0, 2.0, 2.0};
        transforma(_selected_object, matrix);

        system_mode = aux_s;
        edit_mode = aux_e;
      }
      else if(transform_mode == E_CAM)
      {
        float matrix[3] = {KG_STEP_ZOOM, KG_STEP_ZOOM, 0.0};

        int edit_mode_aux = edit_mode;
        edit_mode = E_ESCALAR;
        transformaCamara(_selected_camera, matrix);
        edit_mode = edit_mode_aux;
      }
      else if(_selected_light != 0 && _selected_light->light->lightType == L_SPOT)
      {
        if(_selected_light->light->angle < 180)
        {
          _selected_light->light->angle += KG_STEP_LIGHT_ANGLE;
          if(_selected_light->light->angle > 180) {_selected_light->light->angle = 180;}
          glLightf(_selected_light->light->slot, GL_SPOT_CUTOFF, _selected_light->light->angle);
        }
      }
      break;

    case 'g':
    case 'G':
      if(transform_mode == E_OBJ)
      {
        system_mode = E_GLOBAL;
      }
      else if(_selected_object != 0)
      {
        if(cam_transform_mode != MODO_ANALISIS)
        {
          cam_transform_mode = MODO_ANALISIS;
          mira();
        }
      }
      print_estado();
      break;

    case 'l':
    case 'L':
      if(transform_mode == E_OBJ)
      {
        system_mode = E_LOCAL;
      }
      else
      {
        cam_transform_mode = MODO_VUELO;
      }
      print_estado();
      break;

    case '?':
        print_help();
        print_estado();
        break;

    case 27: /* <ESC> */
        exit(0);
        break;

    case 'm':
    case 'M':
        edit_mode = (edit_mode != E_TRASLADAR) ? E_TRASLADAR : E_APAGADO;
        print_estado();
        break;

    case 'b':
    case 'B':
        edit_mode = (edit_mode != E_ROTAR) ? E_ROTAR : E_APAGADO;
        print_estado();
        break;

    case 't':
    case 'T':
        edit_mode = (edit_mode != E_ESCALAR) ? E_ESCALAR : E_APAGADO;
        print_estado();
        break;

    case 'z':
      break;

    case 'c':
      if(_first_camera != 0)
      {
        _selected_camera = (_selected_camera->cam->next == 0) ? _first_camera : _selected_camera->cam->next;
        mira();
      }
      break;

    case 'C':
      cam_transform_mode = MODO_OBJ;
      print_estado();
      break;

    case 'p':
    case 'P':
      _selected_camera->cam->proyeccion = (_selected_camera->cam->proyeccion == CAM_ORTHOGONAL) ? CAM_PERSPECTIVE : CAM_ORTHOGONAL;
      print_estado();
      break;

    case 'o':
    case 'O':
      transform_mode = E_OBJ;
      print_estado();
      break;

    case 'a':
    case 'A':
      transform_mode = E_LUZ;
      print_estado();
      break;

    case 'k':
    case 'K':
      transform_mode = E_CAM;
      print_estado();
      break;

    case 26:
        if (glutGetModifiers() == GLUT_ACTIVE_CTRL)
        {
          if(_selected_object && _selected_object->matrix_table->next != 0)
          {
            matriz *aux = _selected_object->matrix_table;
            _selected_object->matrix_table = aux->next;

            free(aux);
          }
          mira();
        }
        break;

      default:
        printf("N: %d %c\n", key, key);

    }

    /*In case we have do any modification affecting the displaying of the object, we redraw them*/
    glutPostRedisplay();
}

void transforma(object3d* obj, float *matrix)
{
  glMatrixMode(GL_MODELVIEW);

  if(system_mode == E_GLOBAL)
  {
    glLoadIdentity();
  }
  else
  {
    glLoadMatrixf(obj->matrix_table->value);
  }

  switch(edit_mode)
  {
    case E_TRASLADAR:
      glTranslatef(matrix[EJE_X], matrix[EJE_Y], matrix[EJE_Z]);
      break;

    case E_ROTAR:
      {
        float angle;
        if(matrix[EJE_X] != 0)
        {
          angle = matrix[EJE_X];
          matrix[EJE_X] = 1.0;
        }
        else if(matrix[EJE_Y] != 0)
        {
          angle = matrix[EJE_Y];
          matrix[EJE_Y] = 1.0;
        }
        else
        {
          angle = matrix[EJE_Z];
          matrix[EJE_Z] = 1.0;
        }

        glRotatef(angle, matrix[EJE_X], matrix[EJE_Y], matrix[EJE_Z]);
      }
      break;

    case E_ESCALAR:
      matrix[EJE_X] = (matrix[EJE_X] == 0) ? 1.0 : matrix[EJE_X];
      matrix[EJE_Y] = (matrix[EJE_Y] == 0) ? 1.0 : matrix[EJE_Y];
      matrix[EJE_Z] = (matrix[EJE_Z] == 0) ? 1.0 : matrix[EJE_Z];
      glScalef(matrix[EJE_X], matrix[EJE_Y], matrix[EJE_Z]);
      break;
  }

  if(system_mode == E_GLOBAL)
  {
    glMultMatrixf(obj->matrix_table->value);
  }

  matriz *aux = malloc(sizeof (matriz));
  glGetFloatv(GL_MODELVIEW_MATRIX, aux->value);
  aux->next = obj->matrix_table;
  obj->matrix_table = aux;

  mira();
}

void transformaCamara(object3d* obj, float *matrix)
{
  glMatrixMode(GL_MODELVIEW);

  if(edit_mode != E_ESCALAR)
  {
    if(cam_transform_mode == MODO_ANALISIS)
    {
      switch(edit_mode)
      {
        case E_TRASLADAR:
          {
            if(matrix[EJE_Z] != 0)
            {
              float dist = sqrt(pow(obj->matrix_table->value[12] - _selected_object->matrix_table->value[12], 2) +
                                pow(obj->matrix_table->value[13] - _selected_object->matrix_table->value[13], 2) +
                                pow(obj->matrix_table->value[14] - _selected_object->matrix_table->value[14], 2));

              glLoadMatrixf(obj->matrix_table->value);
              glTranslatef(0, 0, matrix[EJE_Z]);
              if(dist <= 5* KG_STEP_MOVE && matrix[EJE_Z] > 0)
              {
                glTranslatef(0, 0, -matrix[EJE_Z]);
              }

              matriz *aux = malloc(sizeof (matriz));
              glGetFloatv(GL_MODELVIEW_MATRIX, aux->value);
              aux->next = obj->matrix_table;
              obj->matrix_table = aux;
            }
          }
          break;

        case E_ROTAR:
          glLoadIdentity();
          orbita(obj, _selected_object, matrix);
          glMultMatrixf(obj->matrix_table->value);

          matriz *aux = malloc(sizeof (matriz));
          glGetFloatv(GL_MODELVIEW_MATRIX, aux->value);
          aux->next = obj->matrix_table;
          obj->matrix_table = aux;

          break;
      }
    }
    else
    {
      glLoadMatrixf(obj->matrix_table->value);

      switch(edit_mode)
      {
        case E_TRASLADAR:
          glTranslatef(matrix[EJE_X], matrix[EJE_Y], matrix[EJE_Z]);
          break;

        case E_ROTAR:
          {
            float angle;
            if(matrix[EJE_X] != 0)
            {
              angle = matrix[EJE_X];
              matrix[EJE_X] = 1.0;
            }
            else if(matrix[EJE_Y] != 0)
            {
              angle = matrix[EJE_Y];
              matrix[EJE_Y] = 1.0;
            }
            else
            {
              angle = matrix[EJE_Z];
              matrix[EJE_Z] = 1.0;
            }

            glRotatef(angle, matrix[EJE_X], matrix[EJE_Y], matrix[EJE_Z]);
          }
          break;
      }

      matriz *aux = malloc(sizeof (matriz));
      glGetFloatv(GL_MODELVIEW_MATRIX, aux->value);
      aux->next = obj->matrix_table;
      obj->matrix_table = aux;
    }

  }
  else
  {
    matrix[EJE_X] = (matrix[EJE_X] == 0.0) ? 1.0 : matrix[EJE_X];
    matrix[EJE_Y] = (matrix[EJE_Y] == 0.0) ? 1.0 : matrix[EJE_Y];
    matrix[EJE_Z] = (matrix[EJE_Z] == 0.0) ? 1.0 : matrix[EJE_Z];

    if(obj->cam->proyeccion == CAM_ORTHOGONAL)
    {
      _selected_camera->cam->wd*=matrix[EJE_X];
      _selected_camera->cam->he*=matrix[EJE_Y];
    }
    else
    {
      if(matrix[EJE_Z] != 1.0)
      {
        matrix[EJE_X] = matrix[EJE_Y] = matrix[EJE_Z];
      }

      obj->cam->r *= matrix[EJE_X];
      obj->cam->h *= matrix[EJE_Y];
      obj->cam->n *= matrix[EJE_Z];
      obj->cam->far *= matrix[EJE_Z];
    }
  }
}

void transformaLuz(object3d* obj, float *matrix)
{
  glMatrixMode(GL_MODELVIEW);

  switch(_selected_light->light->lightType)
  {
    case L_POINT:
      {
        glLoadIdentity();
        if(edit_mode == E_TRASLADAR)
        {
            glTranslatef(matrix[EJE_X], matrix[EJE_Y], matrix[EJE_Z]);
        }
        glMultMatrixf(obj->matrix_table->value);
        glGetFloatv(GL_MODELVIEW_MATRIX, obj->matrix_table->value);
      }
      break;

    case L_DIRECTIONAL:
      {
        glLoadIdentity();
        if(edit_mode ==  E_ROTAR)
        {
          float angle;
          if(matrix[EJE_X] != 0)
          {
            angle = matrix[EJE_X];
            matrix[EJE_X] = 1.0;
          }
          else if(matrix[EJE_Y] != 0)
          {
            angle = matrix[EJE_Y];
            matrix[EJE_Y] = 1.0;
          }
          else
          {
            angle = matrix[EJE_Z];
            matrix[EJE_Z] = 1.0;
          }

          glRotatef(angle, matrix[EJE_X], matrix[EJE_Y], matrix[EJE_Z]);
        }
        glMultMatrixf(obj->matrix_table->value);
        glGetFloatv(GL_MODELVIEW_MATRIX, obj->matrix_table->value);
      }
      break;

    case L_SPOT:
      if(edit_mode == E_TRASLADAR)
      {
        glLoadIdentity();
        glTranslatef(matrix[EJE_X], matrix[EJE_Y], matrix[EJE_Z]);
        glMultMatrixf(obj->matrix_table->value);
      }
      else if(edit_mode == E_ROTAR)
      {
        glLoadMatrixf(obj->matrix_table->value);

        float angle;
        if(matrix[EJE_X] != 0)
        {
          angle = matrix[EJE_X];
          matrix[EJE_X] = 1.0;
        }
        else if(matrix[EJE_Y] != 0)
        {
          angle = matrix[EJE_Y];
          matrix[EJE_Y] = 1.0;
        }
        else
        {
          angle = matrix[EJE_Z];
          matrix[EJE_Z] = 1.0;
        }
        glRotatef(angle, matrix[EJE_X], matrix[EJE_Y], matrix[EJE_Z]);
      }

      glGetFloatv(GL_MODELVIEW_MATRIX, obj->matrix_table->value);
      break;
  }
}

void speckeyboard(int key, int x, int y)
{
  float matrix[3] = {0.0, 0.0, 0.0}; //Modificadores (x,y,z)

  int actualiza = 0;

  switch(key)
  {
    case GLUT_KEY_LEFT:
      switch(edit_mode)
      {
        case E_TRASLADAR:
        matrix[EJE_X] = KG_STEP_MOVE;
        actualiza = 1;
        break;

        case E_ROTAR:
        matrix[EJE_Y] = KG_STEP_ROTATE;
        actualiza = 1;
        break;

        case E_ESCALAR:
        matrix[EJE_X] = 0.5;
        actualiza = 1;
        break;
      }
      break;

    case GLUT_KEY_UP:
      switch(edit_mode)
      {
        case E_TRASLADAR:
        matrix[EJE_Y] = KG_STEP_MOVE;
        actualiza = 1;
        break;

        case E_ROTAR:
        matrix[EJE_X] = KG_STEP_ROTATE;
        actualiza = 1;
        break;

        case E_ESCALAR:
        matrix[EJE_Y] = 2.0;
        actualiza = 1;
        break;
      }
      break;

    case GLUT_KEY_RIGHT:
      switch(edit_mode)
      {
        case E_TRASLADAR:
        matrix[EJE_X] = -KG_STEP_MOVE;
        actualiza = 1;
        break;

        case E_ROTAR:
        matrix[EJE_Y] = -KG_STEP_ROTATE;
        actualiza = 1;
        break;

        case E_ESCALAR:
        matrix[EJE_X] = 2.0;
        actualiza = 1;
        break;
      }
      break;

    case GLUT_KEY_DOWN:
      switch(edit_mode)
      {
        case E_TRASLADAR:
        matrix[EJE_Y] = -KG_STEP_MOVE;
        actualiza = 1;
        break;

        case E_ROTAR:
        matrix[EJE_X] = -KG_STEP_ROTATE;
        actualiza = 1;
        break;

        case E_ESCALAR:
        matrix[EJE_Y] = 0.5;
        actualiza = 1;
        break;
      }
      break;

    case GLUT_KEY_PAGE_UP:
      switch(edit_mode)
      {
        case E_TRASLADAR:
        matrix[EJE_Z] = KG_STEP_MOVE;
        actualiza = 1;
        break;

        case E_ROTAR:
        matrix[EJE_Z] = KG_STEP_ROTATE;
        actualiza = 1;
        break;

        case E_ESCALAR:
        matrix[EJE_Z] = 2.0;
        actualiza = 1;
        break;
      }
      break;

    case GLUT_KEY_PAGE_DOWN:
      switch(edit_mode)
      {
        case E_TRASLADAR:
        matrix[EJE_Z] = -KG_STEP_MOVE;
        actualiza = 1;
        break;

        case E_ROTAR:
        matrix[EJE_Z] = -KG_STEP_ROTATE;
        actualiza = 1;
        break;

        case E_ESCALAR:
        matrix[EJE_Z] = 0.5;
        actualiza = 1;
        break;
      }
      break;

    case GLUT_KEY_F1:
    case GLUT_KEY_F2:
    case GLUT_KEY_F3:
    case GLUT_KEY_F4:
    case GLUT_KEY_F5:
    case GLUT_KEY_F6:
    case GLUT_KEY_F7:
    case GLUT_KEY_F8:
      toggleLight(key);
      break;

    case GLUT_KEY_F9:
      if (glIsEnabled(GL_LIGHTING))
      {
          glDisable(GL_LIGHTING);
          glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      }
      else
      {
          glEnable(GL_LIGHTING);
          glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      }
      break;

    case GLUT_KEY_F12:
      if(_selected_object != 0)
        toggleShadeModel(_selected_object);
      break;

    default:
      printf("S: %d %c\n", key, key);

  }

  if(actualiza)
  {
    if(transform_mode == E_OBJ)
    {
      transforma(_selected_object, matrix);
    }
    else if(transform_mode == E_CAM)
    {
      transformaCamara(_selected_camera, matrix);
    }
    else
    {
      transformaLuz(_selected_light, matrix);
    }
  }

  /*In case we have do any modification affecting the displaying of the object, we redraw them*/
  glutPostRedisplay();
}
