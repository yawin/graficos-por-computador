#include "definitions.h"
#include "camera.h"
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>

/** EXTERNAL VARIABLES **/

extern GLdouble _window_ratio;
extern GLdouble _ortho_x_min,_ortho_x_max;
extern GLdouble _ortho_y_min,_ortho_y_max;
extern GLdouble _ortho_z_min,_ortho_z_max;

extern object3d *_first_object;
extern object3d *_selected_object;

extern object3d * _selected_camera;
extern int cam_transform_mode;

extern object3d *_first_light;

extern int view_normals;
extern int light_for_selected_object;
extern int light_for_selected_camera;

/**
 * @brief Function to draw the axes
 */
void draw_axes()
{
    /*Draw X axis*/
    glColor3f(KG_COL_X_AXIS_R,KG_COL_X_AXIS_G,KG_COL_X_AXIS_B);
    glBegin(GL_LINES);
    glVertex3d(KG_MAX_DOUBLE,0,0);
    glVertex3d(-1*KG_MAX_DOUBLE,0,0);
    glEnd();
    /*Draw Y axis*/
    glColor3f(KG_COL_Y_AXIS_R,KG_COL_Y_AXIS_G,KG_COL_Y_AXIS_B);
    glBegin(GL_LINES);
    glVertex3d(0,KG_MAX_DOUBLE,0);
    glVertex3d(0,-1*KG_MAX_DOUBLE,0);
    glEnd();
    /*Draw Z axis*/
    glColor3f(KG_COL_Z_AXIS_R,KG_COL_Z_AXIS_G,KG_COL_Z_AXIS_B);
    glBegin(GL_LINES);
    glVertex3d(0,0,KG_MAX_DOUBLE);
    glVertex3d(0,0,-1*KG_MAX_DOUBLE);
    glEnd();
}


/**
 * @brief Callback reshape function. We just store the new proportions of the window
 * @param width New width of the window
 * @param height New height of the window
 */
void reshape(int width, int height) {
    glViewport(0, 0, width, height);
    /*  Take care, the width and height are integer numbers, but the ratio is a GLdouble so, in order to avoid
     *  rounding the ratio to integer values we need to cast width and height before computing the ratio */
    _window_ratio = (GLdouble) width / (GLdouble) height;
}

void object_material (object3d * obj)
{
  if (obj == _selected_object)
  {
      float mat_ambient[] ={ KG_COL_SELECTED_R, KG_COL_SELECTED_G, KG_COL_SELECTED_B, 1.0f };
      float mat_diffuse[] ={ KG_COL_SELECTED_R, KG_COL_SELECTED_G, KG_COL_SELECTED_B, 1.0f  };
      float mat_specular[]={ KG_COL_SELECTED_R, KG_COL_SELECTED_G, KG_COL_SELECTED_B, 1.0f };
      float shine = 60.0;

      glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat_ambient);
      glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat_diffuse);
      glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
      glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shine);
  }
  else
  {
      float mat_ambient[] ={ KG_COL_NONSELECTED_R, KG_COL_NONSELECTED_G, KG_COL_NONSELECTED_B, 1.0f };
      float mat_diffuse[] ={ KG_COL_NONSELECTED_R, KG_COL_NONSELECTED_G, KG_COL_NONSELECTED_B, 1.0f };
      float mat_specular[]={ KG_COL_NONSELECTED_R, KG_COL_NONSELECTED_G, KG_COL_NONSELECTED_B, 1.0f };
      float shine = 60.0;

      glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat_ambient);
      glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat_diffuse);
      glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
      glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shine);
  }
}

void mostrarLuz(object3d *light_obj)
{
  GLint v_index, v, f;
  GLfloat aux[16];

  if(light_obj->light->enabled == 0)
  {
    glDisable(light_obj->light->slot);
    return;
  }

  glEnable(light_obj->light->slot);

  if(light_obj->light->lightIndex == light_for_selected_object)
  {
      if(_selected_object == 0)
      {
        glDisable(light_obj->light->slot);
        return;
      }
      else
      {
        for(int i = 0; i < 16; i++)
          light_obj->matrix_table->value[i] = _selected_object->matrix_table->value[i];
      }
  }

  if(light_obj->light->lightIndex == light_for_selected_camera)
  {
      if(_selected_camera == 0)
      {
        glDisable(light_obj->light->slot);
        return;
      }
      else
      {
        for(int i = 0; i < 16; i++)
          light_obj->matrix_table->value[i] = _selected_camera->matrix_table->value[i];
      }
  }

  GLfloat pos[4] = {light_obj->matrix_table->value[12], light_obj->matrix_table->value[13], light_obj->matrix_table->value[14], (light_obj->light->lightType == L_DIRECTIONAL) ? 0.0 : 1.0};
  if(light_obj->light->lightType == L_DIRECTIONAL)
  {
    pos[0] = light_obj->matrix_table->value[8];
    pos[1] = light_obj->matrix_table->value[9];
    pos[2] = light_obj->matrix_table->value[10];
    pos[3] = 0.0;
  }
  glLightfv(light_obj->light->slot, GL_POSITION, pos);

  if(light_obj->light->lightType == L_SPOT)
  {
    GLfloat dir[3] = {light_obj->matrix_table->value[8], light_obj->matrix_table->value[9], light_obj->matrix_table->value[10]};
    glLightfv(light_obj->light->slot, GL_SPOT_DIRECTION, dir);
  }

  glPushMatrix();
  glMultMatrixf(light_obj->matrix_table->value);
  glShadeModel(GL_FLAT);
  object_material(light_obj);
  for (f = 0; f < light_obj->num_faces; f++)
  {
      glBegin(GL_POLYGON);
      for (v = 0; v < light_obj->face_table[f].num_vertices; v++) {
          v_index = light_obj->face_table[f].vertex_table[v];
          glVertex3d(light_obj->vertex_table[v_index].coord.x,
                  light_obj->vertex_table[v_index].coord.y,
                  light_obj->vertex_table[v_index].coord.z);

      }
      glEnd();
  }
  glPopMatrix();
}

/**
 * @brief Callback display function
 */
void display(void) {
    GLint v_index, v, f;
    object3d *aux_obj = _first_object;

    if(cam_transform_mode != MODO_OBJ)
    {
      updateCameraPosition(_selected_camera->cam, _selected_camera->matrix_table);
    }
    else
    {
      updateCameraPosition(_selected_camera->cam, _selected_object->matrix_table);
      for(int i = 0; i < 16; i++)
        _selected_camera->matrix_table->value[i] = _selected_object->matrix_table->value[i];
    }

    /* Clear the screen */
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    /* Define the projection */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    /*When the window is wider than our original projection plane we extend the plane in the X axis*/
    if(_selected_camera->cam->proyeccion == CAM_ORTHOGONAL)
    {
      _ortho_x_max = _selected_camera->cam->midx + _selected_camera->cam->wd/2;
      _ortho_x_min = _selected_camera->cam->midx - _selected_camera->cam->wd/2;
      _ortho_y_max = _selected_camera->cam->midy + _selected_camera->cam->he/2;
      _ortho_y_min = _selected_camera->cam->midy - _selected_camera->cam->he/2;
      _ortho_z_max = _selected_camera->cam->midz + _selected_camera->cam->zf/2;
      _ortho_z_min = _selected_camera->cam->midz - _selected_camera->cam->zf/2;

      ////////////////////////////////////////////////////////////////////////////////////////////////////////////
      //Si ha cambiado la dimensión horizontal de la ventana
        if ((_ortho_x_max - _ortho_x_min) / (_ortho_y_max - _ortho_y_min) < _window_ratio)
        {
          /* New width */
          GLdouble wd = (_ortho_y_max - _ortho_y_min) * _window_ratio;
          /* Midpoint in the X axis */
          GLdouble midpt = (_ortho_x_min + _ortho_x_max) / 2;
          /*Definition of the projection*/
          glOrtho(midpt - (wd / 2), midpt + (wd / 2), _ortho_y_min, _ortho_y_max, _ortho_z_min, _ortho_z_max);
        }
      //Si ha cambiado la dimensión vertical de la ventana
        else if ((_ortho_x_max - _ortho_x_min) / (_ortho_y_max - _ortho_y_min) > _window_ratio)
        {
          /* New height */
          GLdouble he = (_ortho_x_max - _ortho_x_min) / _window_ratio;
          /* Midpoint in the Y axis */
          GLdouble midpt = (_ortho_y_min + _ortho_y_max) / 2;
          /*Definition of the projection*/
          glOrtho(_ortho_x_min, _ortho_x_max, midpt - (he / 2), midpt + (he / 2), _ortho_z_min, _ortho_z_max);
        }
      //Si la ventana sigue igual
        else
        {
          glOrtho(_ortho_x_min, _ortho_x_max, _ortho_y_min, _ortho_y_max, _ortho_z_min, _ortho_z_max);
        }
      //
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
    else
    {
      glFrustum(-_selected_camera->cam->r, _selected_camera->cam->r,
                -_selected_camera->cam->h, _selected_camera->cam->h,
                 _selected_camera->cam->n, _selected_camera->cam->far);
    }

    //Matriz de cambio del sistema de referencia
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(_selected_camera->cam->matrix);

    /*First, we draw the axes*/
    //draw_axes();

    //COLOCAMOS LAS LUCES
    if(glIsEnabled(GL_LIGHTING))
    {
      object3d *aux_light = _first_light;
      while(aux_light != 0)
      {
        mostrarLuz(aux_light);
        aux_light = aux_light->light->next;
      }
    }

    /*Now each of the objects in the list*/
    while (aux_obj != 0)
    {
        /* Select the color, depending on whether the current object is the selected one or not */
        if(!glIsEnabled(GL_LIGHTING))
        {
          if (aux_obj == _selected_object)
          {
            glColor3f(KG_COL_SELECTED_R,KG_COL_SELECTED_G,KG_COL_SELECTED_B);
          }
          else
          {
            glColor3f(KG_COL_NONSELECTED_R,KG_COL_NONSELECTED_G,KG_COL_NONSELECTED_B);
          }
        }
        else
        {
          glShadeModel(aux_obj->lightType);
          object_material(aux_obj);
        }

        /* Draw the object; for each face create a new polygon with the corresponding vertices */
        //glLoadIdentity();
        glPushMatrix();
        glMultMatrixf(aux_obj->matrix_table->value);

        for (f = 0; f < aux_obj->num_faces; f++) {
            glBegin(GL_POLYGON);

            if(aux_obj->lightType == GL_FLAT)
            {
                glNormal3d(aux_obj->face_table[f].normal.x,
                           aux_obj->face_table[f].normal.y,
                           aux_obj->face_table[f].normal.z);

            }

            for (v = 0; v < aux_obj->face_table[f].num_vertices; v++)
            {
                v_index = aux_obj->face_table[f].vertex_table[v];
                if (aux_obj->lightType == GL_SMOOTH)
                {
                    glNormal3d(aux_obj->vertex_table[v_index].normal.x,
                               aux_obj->vertex_table[v_index].normal.y,
                               aux_obj->vertex_table[v_index].normal.z);
                }
                glVertex3d(aux_obj->vertex_table[v_index].coord.x,
                        aux_obj->vertex_table[v_index].coord.y,
                        aux_obj->vertex_table[v_index].coord.z);

            }
            glEnd();

            if(view_normals != 0)
            {
              glBegin(GL_LINES);
              glVertex3d(aux_obj->vertex_table[v_index].coord.x,
                      aux_obj->vertex_table[v_index].coord.y,
                      aux_obj->vertex_table[v_index].coord.z);
              glVertex3d(aux_obj->vertex_table[v_index].coord.x+aux_obj->vertex_table[v_index].normal.x,
                      aux_obj->vertex_table[v_index].coord.y+aux_obj->vertex_table[v_index].normal.y,
                      aux_obj->vertex_table[v_index].coord.z+aux_obj->vertex_table[v_index].normal.z);
              glEnd();
            }
        }

        glPopMatrix();
        aux_obj = aux_obj->next;
    }
    //glFlush();

    /*Do the actual drawing*/
    glutSwapBuffers();
}
