#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "display.h"
#include "io.h"
#include "definitions.h"
#include "camera.h"
#include "light.h"

/** GLOBAL VARIABLES **/

GLdouble _window_ratio;                     /*Control of window's proportions */
GLdouble _ortho_x_min,_ortho_x_max;         /*Variables for the control of the orthographic projection*/
GLdouble _ortho_y_min ,_ortho_y_max;        /*Variables for the control of the orthographic projection*/
GLdouble _ortho_z_min,_ortho_z_max;         /*Variables for the control of the orthographic projection*/

object3d * _first_object= 0;                /*List of objects*/
object3d * _selected_object = 0;            /*Object currently selected*/

object3d * _first_camera = 0;               /*Listado de camaras que incluye a los objetos*/
object3d * _selected_camera = 0;            /*Camara actual*/

object3d * _first_light = 0;                /*Listado de luces que incluye a los objetos*/
object3d * _selected_light = 0;             /*Luz actual*/
int _selected_light_index = 0;

int view_normals = 0;
int light_for_selected_object = -1;
int light_for_selected_camera = -1;

void setLightForSelectedObject(int index)
{
  if(light_for_selected_camera == index)
  {
    light_for_selected_camera = 0;
  }
  light_for_selected_object = index;
}

void setLightForSelectedCamera(int index)
{
  if(light_for_selected_object == index)
  {
    light_for_selected_object = 0;
  }
  light_for_selected_camera = index;
}

/** GENERAL INITIALIZATION **/
void initialization (){

    /*Initialization of all the variables with the default values*/
    _window_ratio = (GLdouble) KG_WINDOW_WIDTH / (GLdouble) KG_WINDOW_HEIGHT;

    /*Definition of the background color*/
    glClearColor(KG_COL_BACK_R, KG_COL_BACK_G, KG_COL_BACK_B, KG_COL_BACK_A);

    /*Definition of the method to draw the objects*/
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    crearCamara(CAM_ORTHOGONAL);
    _selected_camera = _first_camera;

    /* INICIALIZACION DE LUCES*/
    float ambient[4] = {0.1, 0.1, 0.1, 1.0};
    float diffuse[4] = {0.9, 0.9, 0.9, 1.0};
    crearLuz(0, L_POINT, ambient, diffuse);

    ambient[0] = ambient[1] = ambient[2] = 0.2;
    diffuse[0] = diffuse[1] = diffuse[2] = 0.7;
    crearLuz(1, L_DIRECTIONAL, ambient, diffuse);

    ambient[0] = ambient[1] = ambient[2] = 0.0;
    diffuse[0] = diffuse[1] = diffuse[2] = 0.8;
    crearLuz(2, L_SPOT, ambient, diffuse);
    setLightForSelectedObject(2);

    ambient[0] = ambient[1] = ambient[2] = 0.0;
    diffuse[0] = diffuse[1] = diffuse[2] = 0.8;
    crearLuz(3, L_SPOT, ambient, diffuse);
    setLightForSelectedCamera(3);
}


/** MAIN FUNCTION **/
int main(int argc, char** argv) {

    /*First of all, print the help information*/
    print_help();

    /* glut initializations */
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize(KG_WINDOW_WIDTH, KG_WINDOW_HEIGHT);
    glutInitWindowPosition(KG_WINDOW_X, KG_WINDOW_Y);
    glutCreateWindow(KG_WINDOW_TITLE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); // queremos poligonos opacos

    glEnable(GL_DEPTH_TEST); // activar el test de profundidad (Z-buffer)
    glEnable(GL_LIGHTING);

    /* set the callback functions */
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(speckeyboard);

    /* this initialization has to be AFTER the creation of the window */
    initialization();

    /* start the main loop */
    glutMainLoop();
    return 0;
}
