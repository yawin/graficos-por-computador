#include <stdio.h>
#include "light.h"
#include "load_obj.h"

extern object3d * _first_light;
extern object3d * _selected_light;
extern int _selected_light_index;

void crearLuz(int index, int type, GLfloat *ambient, GLfloat *diffuse)
{
  object3d* obj = malloc(sizeof(object3d));
  char* path = "res/cubo.obj";
  read_wavefront(path, obj);

  obj->matrix_table = malloc(sizeof (matriz));
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glGetFloatv(GL_MODELVIEW_MATRIX, obj->matrix_table->value);

  obj->light = malloc(sizeof (light3d));
  obj->light->lightIndex = index;
  obj->light->lightType = type;

  obj->light->slot = (index == 0) ? GL_LIGHT0 :
                     (index == 1) ? GL_LIGHT1 :
                     (index == 2) ? GL_LIGHT2 :
                     (index == 3) ? GL_LIGHT3 :
                     (index == 4) ? GL_LIGHT4 :
                     (index == 5) ? GL_LIGHT5 :
                     (index == 6) ? GL_LIGHT6 :
                                    GL_LIGHT7;

  for(int i = 0; i < 4; i++)
  {
    obj->light->ambient[i] = ambient[i];
    obj->light->diffuse[i] = diffuse[i];
    obj->light->specular[i] = 1.0;
  }

  obj->light->atenuation[0] = 1.0;
  obj->light->atenuation[1] = 0.0;
  obj->light->atenuation[2] = 0.00001;

  if(type == L_DIRECTIONAL)
  {
    obj->matrix_table->value[8] = 0.0f;
    obj->matrix_table->value[9] = 1.0f;
    obj->matrix_table->value[10] = 0.0f;

    obj->matrix_table->value[12] = 0.0f;
    obj->matrix_table->value[13] = 5000.0f;
    obj->matrix_table->value[14] = 0.0f;
  }

  glLightfv(obj->light->slot, GL_AMBIENT, obj->light->ambient);
  glLightfv(obj->light->slot, GL_DIFFUSE, obj->light->diffuse);
  glLightfv(obj->light->slot, GL_SPECULAR, obj->light->specular);

  GLfloat pos[4] = {obj->matrix_table->value[12], obj->matrix_table->value[13], obj->matrix_table->value[14], 1.0};
  if(type == L_DIRECTIONAL)
  {
    pos[0] = obj->matrix_table->value[8];
    pos[1] = obj->matrix_table->value[9];
    pos[2] = obj->matrix_table->value[10];
    pos[3] = 0.0;
  }
  glLightfv(obj->light->slot, GL_POSITION, pos);

  if(type != L_DIRECTIONAL)
  {
    glLightf(obj->light->slot, GL_CONSTANT_ATTENUATION, obj->light->atenuation[0]);
    glLightf(obj->light->slot, GL_LINEAR_ATTENUATION, obj->light->atenuation[1]);
    glLightf(obj->light->slot, GL_QUADRATIC_ATTENUATION, obj->light->atenuation[2]);

    if(type == L_SPOT)
    {
      GLfloat dir[3] = {obj->matrix_table->value[8], obj->matrix_table->value[9], obj->matrix_table->value[10]};
      glLightfv(obj->light->slot, GL_SPOT_DIRECTION, dir);

      obj->light->angle = 30;
      glLightf(obj->light->slot, GL_SPOT_CUTOFF, obj->light->angle);
  	  glLightf(obj->light->slot,GL_SPOT_EXPONENT, 40);
    }
  }

  obj->light->enabled = 1;

  object3d* cptr = _first_light;
  if(cptr != 0)
  {
    for(; cptr->light->next != 0; cptr = cptr->light->next){}
    cptr->light->next = obj;
    obj->light->last = cptr;
  }
  else
  {
    _first_light = obj;
  }

  obj->light->next = 0;
}

object3d* getLight(int index)
{
  object3d* cptr = _first_light;
  if(cptr != 0)
  {
    for(; cptr != 0; cptr = cptr->light->next)
    {
      if (cptr->light->lightIndex == index)
        break;
    }
  }

  return cptr;
}

void toggleLight(int index)
{
  int realIndex = index - 1;

  object3d* cptr = getLight(realIndex);
  if(cptr != 0)
  {
    cptr->light->enabled = (cptr->light->enabled == 0) ? 1 : 0;
  }
}

void _selectLight(int index)
{
  object3d* cptr = getLight(index);
  if(cptr != 0)
  {
    _selected_light = cptr;
    printf("\nSeleccionada la luz: %d\n", index);
  }
  else
  {
    _selected_light = 0;
    printf("\nLa ranura seleccionada no contiene ninguna luz\n");
  }

  _selected_light_index = index;
}
void selectLight(int index)
{
  int realIndex = index - 49;
  _selectLight(realIndex);
}

void createLight()
{
  if(_selected_light != 0)
  {
    printf("\nNo se puede crear una luz en la ranura seleccionada. \n");
    return;
  }

  printf("\nSelecciona el tipo de luz: [p]oint, [d]irectional o [s]pot -> ");
  char* t = malloc(sizeof (char)*128);
  scanf("%s", t);

  float ambient[4], diffuse[4];
  int type;

  ambient[3] = diffuse[3] = 1.0;

  char c = t[0];
  switch(c)
  {
    case 'P':
    case 'p':
      type = L_POINT;
      ambient[0] = ambient[1] = ambient[2] = 0.1;
      diffuse[0] = diffuse[1] = diffuse[2] = 0.9;
      break;

    case 'D':
    case 'd':
      type = L_DIRECTIONAL;
      ambient[0] = ambient[1] = ambient[2] = 0.2;
      diffuse[0] = diffuse[1] = diffuse[2] = 0.7;
      break;

    case 'S':
    case 's':
      type = L_SPOT;
      ambient[0] = ambient[1] = ambient[2] = 0.2;
      diffuse[0] = diffuse[1] = diffuse[2] = 0.8;
      break;
  }

  crearLuz(_selected_light_index, type, ambient, diffuse);
  _selectLight(_selected_light_index);

  printf("\nCreada luz de tipo %s en el slot %d\n",
        (type == 0) ? "L_POINT" : (type == 1) ? "L_DIRECTIONAL" : "L_SPOT",
        _selected_light_index);
}

void toggleShadeModel(object3d *obj)
{
  obj->lightType = (obj->lightType == GL_FLAT) ? GL_SMOOTH : GL_FLAT;
}
