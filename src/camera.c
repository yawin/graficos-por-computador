#include <GL/glu.h>
#include "camera.h"
#include "load_obj.h"
#include "io.h"

extern int proyeccion;

extern int modoCamara;
extern int system_mode;
extern int edit_mode;

extern object3d * _first_camera;
extern object3d * _selected_camera;

void orbita(object3d* camera, object3d* target, float* ejes)
{
  glTranslatef(target->matrix_table->value[12], target->matrix_table->value[13], target->matrix_table->value[14]);

  float angle;
  if(ejes[EJE_X] != 0)
  {
    angle = -ejes[EJE_X];
    ejes[EJE_X] = 1.0;
  }
  else if(ejes[EJE_Y] != 0)
  {
    angle = -ejes[EJE_Y];
    ejes[EJE_Y] = 1.0;
  }
  else
  {
    angle = -ejes[EJE_Z];
    ejes[EJE_Z] = 1.0;
  }

  if(ejes[EJE_X] == 1.0)
  {
    glRotatef(angle, camera->matrix_table->value[0], camera->matrix_table->value[1], camera->matrix_table->value[2]);
  }
  else if(ejes[EJE_Y] == 1.0)
  {
    glRotatef(angle, camera->matrix_table->value[4], camera->matrix_table->value[5], camera->matrix_table->value[6]);
  }
  else
  {
    glRotatef(angle, camera->matrix_table->value[8], camera->matrix_table->value[9], camera->matrix_table->value[10]);
  }

  glTranslatef(-target->matrix_table->value[12], -target->matrix_table->value[13], -target->matrix_table->value[14]);
}

void updateCameraPosition(camera3d* cam, matriz* target)
{
  glLoadMatrixf(target->value);
  glTranslatef(0, 0, -KG_STEP_MOVE);

  float aux[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, aux);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  gluLookAt(aux[12], aux[13], aux[14],
    target->value[12], target->value[13], target->value[14],
    target->value[4], target->value[5], target->value[6]);
  glGetFloatv(GL_MODELVIEW_MATRIX, cam->matrix);
}

void mirarObjeto(object3d* camera, object3d* target)
{
  float matrix[16];

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  gluLookAt(camera->matrix_table->value[12], camera->matrix_table->value[13], camera->matrix_table->value[14], //Punto en el que se encuentra la camara
            target->matrix_table->value[12], target->matrix_table->value[13], target->matrix_table->value[14], //Punto al que miro (el objeto seleccionado)
            camera->matrix_table->value[4],  camera->matrix_table->value[5],  camera->matrix_table->value[6]); //Mantenemos la vertical de la camara

  glGetFloatv(GL_MODELVIEW_MATRIX, matrix);

  //Necesito actualizar los vectores de la camara pues estos pueden sufrir modificaciones
  //ejeX
    camera->matrix_table->value[0] = -matrix[0];
    camera->matrix_table->value[1] = -matrix[4];
    camera->matrix_table->value[2] = -matrix[8];
  //EjeY
    camera->matrix_table->value[4] = matrix[1];
    camera->matrix_table->value[5] = matrix[5];
    camera->matrix_table->value[6] = matrix[9];
  //EjeZ
    camera->matrix_table->value[8] = -matrix[2];
    camera->matrix_table->value[9] = -matrix[6];
    camera->matrix_table->value[10] = -matrix[10];
}

void crearCamara(int proyeccion)
{
  object3d* obj = malloc(sizeof(object3d));
  char* path = "res/cubo.obj";
  read_wavefront(path, obj);

  obj->matrix_table = malloc(sizeof (matriz));

  obj->cam = malloc(sizeof(camera3d));
  obj->cam->proyeccion = proyeccion;
  obj->cam->next = 0;
  obj->cam->last = 0;

  object3d* cptr = _first_camera;
  if(cptr != 0)
  {
    for(; cptr->cam->next != 0; cptr = cptr->cam->next){}
    cptr->cam->next = obj;
    obj->cam->last = cptr;
  }
  else
  {
    _first_camera = obj;
  }

  obj->cam->next = 0;

  obj->cam->far = 1000;
  obj->cam->n = 8;
  obj->cam->r = 8;//KG_WINDOW_WIDTH/50.0;
  obj->cam->h = 8;//KG_WINDOW_HEIGHT/50.0;

  obj->cam->wd = KG_ORTHO_X_MAX_INIT - KG_ORTHO_X_MIN_INIT;
  obj->cam->he = KG_ORTHO_Y_MAX_INIT - KG_ORTHO_Y_MIN_INIT;
  obj->cam->zf = KG_ORTHO_Z_MAX_INIT - KG_ORTHO_Z_MIN_INIT;
  obj->cam->midx = (KG_ORTHO_X_MAX_INIT + KG_ORTHO_X_MIN_INIT) / 2;
  obj->cam->midy = (KG_ORTHO_Y_MAX_INIT + KG_ORTHO_Y_MIN_INIT) / 2;
  obj->cam->midz = (KG_ORTHO_Z_MAX_INIT + KG_ORTHO_Z_MIN_INIT) / 2;

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glGetFloatv(GL_MODELVIEW_MATRIX, obj->matrix_table->value);
  updateCameraPosition(obj->cam, obj->matrix_table);
}

void borraCamara(object3d* obj)
{
  if(obj->cam->last != 0)
  {
    obj->cam->last->cam->next = obj->cam->next;
  }
  if(obj->cam->next != 0)
  {
    obj->cam->next->cam->last = obj->cam->last;
  }

  free(obj->cam);
}
