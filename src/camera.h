#include "definitions.h"
#include <stdlib.h>
#ifndef camera_h
#define camera_h

/*void cameraPosition(object3d* obj, float* pos_ptr);
void cameraVertical(object3d* obj, float* vert_ptr);
void cameraLookPoint(object3d* obj, float* look_ptr);*/

void crearCamara();
void mirarObjeto(object3d* camera, object3d* target);
void borraCamara(object3d* obj);
void orbita(object3d* camera, object3d* target, float* ejes);
void updateCameraPosition(camera3d* cam, matriz* target);

#endif /* camera_h */
