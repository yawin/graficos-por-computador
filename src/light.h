#include "definitions.h"
#include <stdlib.h>
#ifndef light_h
#define light_h

void crearLuz(int index, int type, GLfloat *ambient, GLfloat *diffuse);
void toggleLight(int index);
void selectLight(int index);
void createLight();
void toggleShadeModel(object3d *obj);

#endif /* light_h */
