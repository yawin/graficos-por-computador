#include "vectores.h"
#include <math.h>

void CalcularNormales(object3d *objetoPtr)
{
    vector3 v1,v2;
    GLint p[3];
    GLdouble modulo;

    //Inicializar todas las normales de los vertices a 0
    for(int i = 0; i < objetoPtr->num_vertices; i++){
        objetoPtr->vertex_table[i].normal.x = 0;
        objetoPtr->vertex_table[i].normal.y = 0;
        objetoPtr->vertex_table[i].normal.z = 0;
    }

    for(int i = 0; i < objetoPtr->num_faces; i++)
    {
        //obtener los tres vertices
        p[0] = objetoPtr->face_table[i].vertex_table[0];
        p[1] = objetoPtr->face_table[i].vertex_table[1];
        p[2] = objetoPtr->face_table[i].vertex_table[2];
        //primer vector
        v1.x = objetoPtr->vertex_table[p[2]].coord.x - objetoPtr->vertex_table[p[1]].coord.x;
        v1.y = objetoPtr->vertex_table[p[2]].coord.y - objetoPtr->vertex_table[p[1]].coord.y;
        v1.z = objetoPtr->vertex_table[p[2]].coord.z - objetoPtr->vertex_table[p[1]].coord.z;
        //Segundo vector
        v2.x = objetoPtr->vertex_table[p[0]].coord.x - objetoPtr->vertex_table[p[1]].coord.x;
        v2.y = objetoPtr->vertex_table[p[0]].coord.y - objetoPtr->vertex_table[p[1]].coord.y;
        v2.z = objetoPtr->vertex_table[p[0]].coord.z - objetoPtr->vertex_table[p[1]].coord.z;
        //vector normal
        objetoPtr->face_table[i].normal.x = v1.y * v2.z - v1.z * v2.y;
        objetoPtr->face_table[i].normal.y = v1.z * v2.x - v1.x * v2.z;
        objetoPtr->face_table[i].normal.z = v1.x * v2.y - v1.y * v2.x;
        //hacerlo unitario
        modulo = sqrt(pow(objetoPtr->face_table[i].normal.x, 2) + pow(objetoPtr->face_table[i].normal.y, 2) + pow(objetoPtr->face_table[i].normal.z, 2));
        objetoPtr->face_table[i].normal.x = objetoPtr->face_table[i].normal.x / modulo;
        objetoPtr->face_table[i].normal.y = objetoPtr->face_table[i].normal.y / modulo;
        objetoPtr->face_table[i].normal.z = objetoPtr->face_table[i].normal.z / modulo;
        //Sumar ese vector a las normales de cada uno de sus vertices
        for(int j = 0; j < objetoPtr->face_table[i].num_vertices; j++)
        {
            objetoPtr->vertex_table[objetoPtr->face_table[i].vertex_table[j]].normal.x += objetoPtr->face_table[i].normal.x;
            objetoPtr->vertex_table[objetoPtr->face_table[i].vertex_table[j]].normal.y += objetoPtr->face_table[i].normal.y;
            objetoPtr->vertex_table[objetoPtr->face_table[i].vertex_table[j]].normal.z += objetoPtr->face_table[i].normal.z;
        }

    }
    //Hacer unitarias las normales de cada uno de los vertices
    for(int i = 0; i < objetoPtr->num_vertices; i++)
    {
        modulo = sqrt(pow(objetoPtr->vertex_table[i].normal.x, 2) + pow(objetoPtr->vertex_table[i].normal.y, 2) + pow(objetoPtr->vertex_table[i].normal.z, 2));
        objetoPtr->vertex_table[i].normal.x /= modulo;
        objetoPtr->vertex_table[i].normal.y /= modulo;
        objetoPtr->vertex_table[i].normal.z /= modulo;
    }
}
